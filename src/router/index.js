import Vue from 'vue'
import Router from 'vue-router'
import Index from '@/components/Index'
import Trek from '@/components/Trek'
import Allplaces from '@/components/Allplaces'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Index',
      component: Index
    },
    {
      path: '/trek/kilimajaro',
      name: 'trek',
      component: Trek
    },
    {
      path: '/trek/other',
      name: 'allplaces',
      component: Allplaces
    },
  ]
})
